#-------------------------------------------------
#
# Project created by QtCreator 2019-05-29T19:26:22
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MainFrame
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x056000    # disables all the APIs deprecated before Qt 6.0.0
DEFINES += _USE_QT_
#DEFINES += __WITH_CHAISCRIPT__
DEFINES += __WITH_LUA__

CONFIG += c++17

SOURCES += \
        CvWidget/cvwidgetui.cpp \
        ED_Lib-master/ED.cpp \
        ED_Lib-master/EDCircles.cpp \
        ED_Lib-master/EDColor.cpp \
        ED_Lib-master/EDLines.cpp \
        ED_Lib-master/EDPF.cpp \
        ED_Lib-master/NFA.cpp \
        OCR/PassImageProc.cpp \
        OCR/ocrtest.cpp \
        QtOpenCV/cvmatandqimage.cpp \
        FeatureDetection/featuredetector.cpp \
        Script/ChaiScriptSyntaxHighlighter.cpp \
        Script/chaiscript.cpp \
        Script/tolua.cpp\
        gbaseclass.cpp \
        main.cpp \
        mainui.cpp  \
        msghandlerwapper.cpp \
        shape_based_matching/line2Dup.cpp

HEADERS += \
        CvWidget/cvwidgetui.h \
        ED_Lib-master/ED.h \
        ED_Lib-master/EDCircles.h \
        ED_Lib-master/EDColor.h \
        ED_Lib-master/EDLib.h \
        ED_Lib-master/EDLines.h \
        ED_Lib-master/EDPF.h \
        ED_Lib-master/NFA.h \
        OCR/PassImageProc.h \
        OCR/ocrtest.h \
        QtOpenCV/asmopencv.h \
        QtOpenCV/cvmatandqimage.h \
        FeatureDetection/featuredetector.h \
        Script/ChaiScriptSyntaxHighlighter.hpp \
        Script/chaiscript.h \
        Script/tolua.h\
        gbaseclass.h \
        mainui.h  \
        msghandlerwapper.h \
        shape_based_matching/MIPP/math/avx512_mathfun.h \
        shape_based_matching/MIPP/math/avx512_mathfun.hxx \
        shape_based_matching/MIPP/math/avx_mathfun.h \
        shape_based_matching/MIPP/math/avx_mathfun.hxx \
        shape_based_matching/MIPP/math/neon_mathfun.h \
        shape_based_matching/MIPP/math/neon_mathfun.hxx \
        shape_based_matching/MIPP/math/sse_mathfun.h \
        shape_based_matching/MIPP/math/sse_mathfun.hxx \
        shape_based_matching/MIPP/mipp.h \
        shape_based_matching/MIPP/mipp_impl_AVX.hxx \
        shape_based_matching/MIPP/mipp_impl_AVX512.hxx \
        shape_based_matching/MIPP/mipp_impl_NEON.hxx \
        shape_based_matching/MIPP/mipp_impl_SSE.hxx \
        shape_based_matching/MIPP/mipp_object.hxx \
        shape_based_matching/MIPP/mipp_scalar_op.h \
        shape_based_matching/MIPP/mipp_scalar_op.hxx \
        shape_based_matching/line2Dup.h

FORMS += \
        CvWidget/cvwidgetui.ui \
        FeatureDetection/featuredetector.ui \
        mainui.ui \
        OCR/ocrtest.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32:!win32-g++: {
message("Using win32 configuration")

OPENCV_PATH = $(OPENCV_INCLUDE) # Note: update with the correct OpenCV version
QMAKE_CXXFLAGS += /bigobj
LIBS_PATH = $(OPENCV_LIB) #project compiled using Visual C++ 2010 32bit compiler

    CONFIG(debug, debug|release) {
    LIBS     += -L$$LIBS_PATH \
    -lopencv_bgsegm343d\
    -lopencv_bioinspired343d\
    -lopencv_calib3d343d\
    -lopencv_ccalib343d\
    -lopencv_core343d\
    -lopencv_datasets343d\
    -lopencv_dnn343d\
    -lopencv_dnn_objdetect343d\
    -lopencv_dpm343d\
    -lopencv_face343d\
    -lopencv_features2d343d\
    -lopencv_flann343d\
    -lopencv_fuzzy343d\
    -lopencv_hfs343d\
    -lopencv_highgui343d\
    -lopencv_imgcodecs343d\
    -lopencv_imgproc343d\
    -lopencv_img_hash343d\
    -lopencv_line_descriptor343d\
    -lopencv_ml343d\
    -lopencv_objdetect343d\
    -lopencv_optflow343d\
    -lopencv_phase_unwrapping343d\
    -lopencv_photo343d\
    -lopencv_plot343d\
    -lopencv_reg343d\
    -lopencv_rgbd343d\
    -lopencv_saliency343d\
    -lopencv_shape343d\
    -lopencv_stereo343d\
    -lopencv_stitching343d\
    -lopencv_structured_light343d\
    -lopencv_superres343d\
    -lopencv_surface_matching343d\
    -lopencv_text343d\
    -lopencv_video343d\
    -lopencv_videoio343d\
    -lopencv_videostab343d\
    -lopencv_xfeatures2d343d\
    -lopencv_ximgproc343d\
    -lopencv_xobjdetect343d\
    -lopencv_xphoto343d

    LIBS     += -L$$PWD/CvImageShow/Debug -lQtCvImageShow
    }

    CONFIG(release, debug|release) {
    LIBS     += -L$$LIBS_PATH \
    -lopencv_bgsegm343\
    -lopencv_bioinspired343\
    -lopencv_calib3d343\
    -lopencv_ccalib343\
    -lopencv_core343\
    -lopencv_datasets343\
    -lopencv_dnn343\
    -lopencv_dnn_objdetect343\
    -lopencv_dpm343\
    -lopencv_face343\
    -lopencv_features2d343\
    -lopencv_flann343\
    -lopencv_fuzzy343\
    -lopencv_hfs343\
    -lopencv_highgui343\
    -lopencv_imgcodecs343\
    -lopencv_imgproc343\
    -lopencv_img_hash343\
    -lopencv_line_descriptor343\
    -lopencv_ml343\
    -lopencv_objdetect343\
    -lopencv_optflow343\
    -lopencv_phase_unwrapping343\
    -lopencv_photo343\
    -lopencv_plot343\
    -lopencv_reg343\
    -lopencv_rgbd343\
    -lopencv_saliency343\
    -lopencv_shape343\
    -lopencv_stereo343\
    -lopencv_stitching343\
    -lopencv_structured_light343\
    -lopencv_superres343\
    -lopencv_surface_matching343\
    -lopencv_text343\
    -lopencv_video343\
    -lopencv_videoio343\
    -lopencv_videostab343\
    -lopencv_xfeatures2d343\
    -lopencv_ximgproc343\
    -lopencv_xobjdetect343\
    -lopencv_xphoto343

    LIBS     += -L$$PWD/CvImageShow/Release -lQtCvImageShow
    }
}

win32-g++: {
message("Using win32-g++ configuration")

QMAKE_CXXFLAGS += -Wa,-mbig-obj
#QMAKE_LFLAGS += -Wa,-mbig-obj

#QMAKE_CXXFLAGS_DEBUG += -O1

MINGW64_PATH = C:/building/msys64/mingw64

OPENCV_PATH = $$MINGW64_PATH/opencv_410/include
LIBS_PATH = $$MINGW64_PATH/opencv_410/x64/mingw/lib

LIBS     += -L$$LIBS_PATH \
    -lopencv_aruco410.dll\
    -lopencv_bgsegm410.dll\
    -lopencv_bioinspired410.dll\
    -lopencv_calib3d410.dll\
    -lopencv_ccalib410.dll\
    -lopencv_core410.dll\
    -lopencv_datasets410.dll\
    -lopencv_dnn_objdetect410.dll\
    -lopencv_dnn410.dll\
    -lopencv_dpm410.dll\
    -lopencv_face410.dll\
    -lopencv_features2d410.dll\
    -lopencv_flann410.dll\
    -lopencv_fuzzy410.dll\
    -lopencv_hfs410.dll\
    -lopencv_highgui410.dll\
    -lopencv_img_hash410.dll\
    -lopencv_imgcodecs410.dll\
    -lopencv_imgproc410.dll\
    -lopencv_line_descriptor410.dll\
    -lopencv_ml410.dll\
    -lopencv_objdetect410.dll\
    -lopencv_optflow410.dll\
    -lopencv_phase_unwrapping410.dll\
    -lopencv_photo410.dll\
    -lopencv_plot410.dll\
    -lopencv_quality410.dll\
    -lopencv_reg410.dll\
    -lopencv_rgbd410.dll\
    -lopencv_saliency410.dll\
    -lopencv_shape410.dll\
    -lopencv_stereo410.dll\
    -lopencv_stitching410.dll\
    -lopencv_structured_light410.dll\
    -lopencv_superres410.dll\
    -lopencv_surface_matching410.dll\
    -lopencv_text410.dll\
    -lopencv_tracking410.dll\
    -lopencv_video410.dll\
    -lopencv_videoio410.dll\
    -lopencv_videostab410.dll\
    -lopencv_xfeatures2d410.dll\
    -lopencv_ximgproc410.dll\
    -lopencv_xobjdetect410.dll\
    -lopencv_xphoto410.dll

    CONFIG(debug, debug|release) {
        LIBS     += -L$$PWD/CvImageShow/win32-g++/Debug -lQtCvImageShow.dll
    }
        CONFIG(release, debug|release) {
        LIBS     += -L$$PWD/CvImageShow/win32-g++/Release -lQtCvImageShow.dll
    }

    INCLUDEPATH += $$MINGW64_PATH/chaiscript/include
    LIBS     += -L$$MINGW64_PATH/chaiscript/lib/chaiscript -llibchaiscript_stdlib-6.1.0  -llibstl_extra -llibtest_module

    INCLUDEPATH += $$MINGW64_PATH/luajit/include/luajit-2.0
    LIBS     += -L$$MINGW64_PATH/luajit/lib -lluajit-5.1
}

unix : {
message("Using unix configuration")

OPENCV_PATH = /usr/include/opencv4/
LIBS_PATH = /usr/lib/

LIBS     += -L$$LIBS_PATH \
            -lopencv_aruco\
            -lopencv_bgsegm\
            -lopencv_bioinspired\
            -lopencv_calib3d\
            -lopencv_ccalib\
            -lopencv_core\
            -lopencv_cvv\
            -lopencv_datasets\
            -lopencv_dnn\
            -lopencv_dnn_objdetect\
            -lopencv_dpm\
            -lopencv_face\
            -lopencv_features2d\
            -lopencv_flann\
            -lopencv_fuzzy\
            -lopencv_gapi\
            -lopencv_hfs\
            -lopencv_highgui\
            -lopencv_img_hash\
            -lopencv_imgcodecs\
            -lopencv_imgproc\
            -lopencv_line_descriptor\
            -lopencv_ml\
            -lopencv_objdetect\
            -lopencv_optflow\
            -lopencv_phase_unwrapping\
            -lopencv_photo\
            -lopencv_plot\
            -lopencv_quality\
            -lopencv_reg\
            -lopencv_rgbd\
            -lopencv_saliency\
            -lopencv_shape\
            -lopencv_stereo\
            -lopencv_stitching\
            -lopencv_structured_light\
            -lopencv_superres\
            -lopencv_surface_matching\
            -lopencv_text\
            -lopencv_tracking\
            -lopencv_video\
            -lopencv_videoio\
            -lopencv_videostab\
            -lopencv_xfeatures2d\
            -lopencv_ximgproc\
            -lopencv_xobjdetect\
            -lopencv_xphoto

    CONFIG(debug, debug|release) {
        LIBS     += -L$$PWD/CvImageShow/g++/Debug -lQtCvImageShow
    }
        CONFIG(release, debug|release) {
        LIBS     += -L$$PWD/CvImageShow/g++/Release -lQtCvImageShow
    }

    LIBS     += -ldl
    LIBS     += -L$$LIBS_PATH/chaiscript/ -lchaiscript_stdlib-6.1.0  -lstl_extra
}

INCLUDEPATH += \
    $$OPENCV_PATH \ #core module
    $$PWD/CvImageShow/include

message("OpenCV path: $$OPENCV_PATH")
message("Includes path: $$INCLUDEPATH")
message("Libraries: $$LIBS")
