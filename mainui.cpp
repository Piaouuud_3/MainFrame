﻿#include "mainui.h"
#include "ui_mainui.h"

#include <QtWidgets/QGridLayout>
#include "CvWidget/cvwidgetui.h"
#include "OCR/ocrtest.h"
#include "FeatureDetection/featuredetector.h"

#include "msghandlerwapper.h"

MainUi::MainUi(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainUi)
{
    LogCount = 0;
    ui->setupUi(this);

    connect(MsgHandlerWapper::instance(), SIGNAL(LogMessageSig(const QtMsgType, const QString, const QString &)),
            this, SLOT(LogMessageslot(const QtMsgType, const QString, const QString &)) );

    pCvwidgetui = nullptr;
    pOcrTest = nullptr;
    pFeatureDetect = nullptr;

    InitCvWidget();

    ui->mdiArea->tileSubWindows();
}

MainUi::~MainUi()
{
    delete ui;
}

void MainUi::LogMessageslot(const QtMsgType type, const QString current_date_time, const QString &msg)
{
    ui->LogTableWidget->insertRow(0);
    QTableWidgetItem *LogCol0 = nullptr;
    QTableWidgetItem *LogCol1 = new QTableWidgetItem(current_date_time);
    QTableWidgetItem *LogCol2 = new QTableWidgetItem(msg);

    switch(type)
    {
    case QtInfoMsg:
        LogCol0 = new QTableWidgetItem("Info");
        LogCol0->setBackgroundColor(QColor(Qt::lightGray));
        LogCol1->setBackgroundColor(QColor(Qt::lightGray));
        LogCol2->setBackgroundColor(QColor(Qt::lightGray));
        break;
    case QtDebugMsg:
        LogCol0 = new QTableWidgetItem("Debug");
        LogCol0->setBackgroundColor(QColor(Qt::gray));
        LogCol1->setBackgroundColor(QColor(Qt::gray));
        LogCol2->setBackgroundColor(QColor(Qt::gray));
        break;

    case QtWarningMsg:
        LogCol0 = new QTableWidgetItem("Warning");
        LogCol0->setBackgroundColor(QColor(Qt::magenta));
        LogCol1->setBackgroundColor(QColor(Qt::magenta));
        LogCol2->setBackgroundColor(QColor(Qt::magenta));
        break;

    case QtCriticalMsg:
        LogCol0 = new QTableWidgetItem("Critical");
        LogCol0->setBackgroundColor(QColor(Qt::darkRed));
        LogCol1->setBackgroundColor(QColor(Qt::darkRed));
        LogCol2->setBackgroundColor(QColor(Qt::darkRed));
        break;

    case QtFatalMsg:
        LogCol0 = new QTableWidgetItem("Fatal");
        LogCol0->setBackgroundColor(QColor(Qt::red));
        LogCol1->setBackgroundColor(QColor(Qt::red));
        LogCol2->setBackgroundColor(QColor(Qt::red));
        break;

    default:
        LogCol0 = new QTableWidgetItem("Debug");
        LogCol0->setBackgroundColor(QColor(Qt::gray));
        LogCol1->setBackgroundColor(QColor(Qt::gray));
        LogCol2->setBackgroundColor(QColor(Qt::gray));
        break;
    }

    ui->LogTableWidget->setItem(0, 0, LogCol0 );
    ui->LogTableWidget->setItem(0, 1, LogCol1 );
    ui->LogTableWidget->setItem(0, 2, LogCol2 );

    if( ui->LogTableWidget->rowCount() > 1000 )
    {
        ui->LogTableWidget->removeRow(ui->LogTableWidget->rowCount());
    }
}

void MainUi::InitCvWidget()
{
    if(pCvwidgetui == nullptr)
    {
        pCvwidgetui = new cvwidgetui();
        //pCvwidgetui->setObjectName(QStringLiteral("widget"));
        pCvwidgetui->setObjectName(QString::fromUtf8("cvWidget"));
        ui->mdiArea->addSubWindow(pCvwidgetui);
    }

    if(pOcrTest == nullptr)
    {
        pOcrTest = new OcrTest();
        pOcrTest->setObjectName(QString::fromUtf8("OcrTest"));
        ui->mdiArea->addSubWindow(pOcrTest);

        if(pCvwidgetui != nullptr)
        {
           connect(pOcrTest, &OcrTest::ShowImageSig, pCvwidgetui, &cvwidgetui::ShowImageSlot);
           connect(pOcrTest, &OcrTest::UpdateIconListSig, pCvwidgetui, &cvwidgetui::UpdateIconListSlot);
           connect( pCvwidgetui, &cvwidgetui::SlectectImageToShowSig, pOcrTest, &OcrTest::SlectectImageToShowSlot);

           //connect(pOcrTest, SIGNAL(ShowImageSig), pCvwidgetui, SLOT(ShowImageSlot));
           //connect(pOcrTest, SIGNAL(UpdateIconListSig), pCvwidgetui, SLOT(UpdateIconListSlot));
           //connect(pCvwidgetui, SIGNAL(SlectectImageToShowSig), pOcrTest, SLOT(SlectectImageToShowSlot));
        }
    }

    if(pFeatureDetect == nullptr)
    {
        pFeatureDetect = new FeatureDetect();
        pFeatureDetect->setObjectName(QString::fromUtf8("特征点检测"));
        ui->mdiArea->addSubWindow(pFeatureDetect);

        if(pCvwidgetui != nullptr)
        {
           connect(pFeatureDetect, &FeatureDetect::ShowImageSig, pCvwidgetui, &cvwidgetui::ShowImageSlot);
           connect(pFeatureDetect, &FeatureDetect::UpdateIconListSig, pCvwidgetui, &cvwidgetui::UpdateIconListSlot);
           connect( pCvwidgetui, &cvwidgetui::SlectectImageToShowSig, pFeatureDetect, &FeatureDetect::SlectectImageToShowSlot);

           //connect(pOcrTest, SIGNAL(ShowImageSig), pCvwidgetui, SLOT(ShowImageSlot));
           //connect(pOcrTest, SIGNAL(UpdateIconListSig), pCvwidgetui, SLOT(UpdateIconListSlot));
           //connect(pCvwidgetui, SIGNAL(SlectectImageToShowSig), pOcrTest, SLOT(SlectectImageToShowSlot));
        }
    }
}

void MainUi::on_tile_triggered()
{
    ui->mdiArea->tileSubWindows();
}
