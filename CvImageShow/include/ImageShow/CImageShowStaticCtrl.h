﻿/*!
 * \file	CImageShowStaticCtrl.h.
 *
 * \brief	该文件中定义了显示图像相关的头文件，该类组包括了显示OpencCV中Mat图像和绘制各种Shape图像的操作
 * 			CvImageShow包括了如下类:
 * 				CImageShow: 
 * 				CImageShowStaticCtrl
 * 			可以支持MFC和Qt两种窗体程序需要
 */

#pragma once

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <memory>

/*
*	_USE_QT_ 宏决定是支持MFC程序还是为Qt程序使用
*/
#include "ImageShow/CvImageShow.h"
#include "ImageShow/CImageShow.h"

#ifdef _USE_QT_
#include <functional>
#include <QtWidgets/QLabel>
#include <QtGui/QMouseEvent>
#include <QtWidgets/QOpenGLWidget>
#define __USE_GL__ 0
#endif

namespace CvImageShow
{
#ifndef _USE_QT_
	class CV_EXPORT CImageShowStaticCtrl : public CStatic, public CImageShow
#else
#if __USE_GL__
    class CV_EXPORT CImageShowStaticCtrl : public QOpenGLWidget, public CImageShow
#else
    class CV_EXPORT CImageShowStaticCtrl : public QLabel, public CImageShow
#endif
#endif
	{

#ifndef _USE_QT_
		DECLARE_DYNAMIC(CImageShowStaticCtrl)
	protected:
		DECLARE_MESSAGE_MAP()
	public:
		afx_msg void OnDestroy();
		afx_msg BOOL OnEraseBkgnd(CDC* pDC);
		afx_msg UINT OnGetDlgCode();

		afx_msg void OnSetFocus(CWnd* pOldWnd);
		afx_msg void OnKillFocus(CWnd* pNewWnd);

		afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
		afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
		afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);

		afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
		afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
		afx_msg void OnRButtonDblClk(UINT nFlags, CPoint point);

		afx_msg void OnMouseMove(UINT nFlags, CPoint point);
		afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);

		afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
		afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);

		afx_msg void OnTimer(UINT_PTR nIDEvent);

		afx_msg void OnPaint();
#else
		Q_OBJECT
	public:
		explicit CImageShowStaticCtrl(QWidget *parent = 0);

#ifdef __cplusplus      
        void SetMouseDoubleClickFun(std::function<bool (const cv::Point2f)> Fun)
        {
            m_MouseDoubleClickFun = Fun;
        }

        void SetMouseReleaseFun(std::function<bool (const cv::Point2f)> Fun)
        {
            m_MouseReleaseFun = Fun;
        }

private:
        std::function<bool (const cv::Point2f)> m_MouseDoubleClickFun;
        std::function<bool (const cv::Point2f)> m_MouseReleaseFun;
#endif
	signals:

	public slots:

	private:
		virtual void paintEvent(QPaintEvent* event);

		virtual void enterEvent(QEvent* event);
		virtual void leaveEvent(QEvent* event);

		virtual void focusInEvent(QFocusEvent* event);
		virtual void focusOutEvent(QFocusEvent* event);

		virtual void mousePressEvent(QMouseEvent* event);
		virtual void mouseReleaseEvent(QMouseEvent *event);

		virtual void mouseDoubleClickEvent(QMouseEvent *event);
		virtual void mouseMoveEvent(QMouseEvent *event);
		virtual void wheelEvent(QWheelEvent *event);

		virtual void resizeEvent(QResizeEvent* event);
#endif
	};	// end for class CImageShowStaticCtrl
}	// namespace CvImageShow

#ifdef _USE_QT_
using namespace CvImageShow;
#endif
