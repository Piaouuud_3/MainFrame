﻿/*!
 * \file	CvImageShow.h.
 *
 * \brief	该文件中定义了显示图像相关的头文件，该类组包括了显示OpencCV中Mat图像和绘制各种Shape图像的操作
 * 			CvImageShow包括了如下类:
 * 				CImageShow: 
 * 				CImageShowStaticCtrl
 * 			可以支持MFC和Qt两种窗体程序需要
 */
//#pragma once
#ifndef __CVIMAGESHOW_DEFINE_H__
#define __CVIMAGESHOW_DEFINE_H__

#ifndef CVIMAGE_DLL
        #ifndef CV_EXPORT
        #define CV_EXPORT
        #endif
#else
        #ifndef CV_EXPORT
                #define CV_EXPORT
        #endif
#endif


#ifdef _MSC_VER		// 针对使用MSBuild环境编译下编译MFC还是Qt程序的情况

#ifndef CVIMAGE_DLL
        #ifndef CV_EXPORT
        #define CV_EXPORT __declspec(dllimport)
        #endif
#else
        #ifndef CV_EXPORT
                #define CV_EXPORT __declspec(dllexport)
        #endif
#endif

#ifndef _USE_QT_
// 使用GDI+来完成图像与各种Shape的绘制
#include <gdiplus.h>
using namespace Gdiplus;
#pragma comment( lib, "gdiplus.lib" )
#endif
#endif

#endif
