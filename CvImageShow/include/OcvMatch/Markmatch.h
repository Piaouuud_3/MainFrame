﻿#pragma once

#define  USEOPENCV 1

#if USEOPENCV
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "../ImageShow/CImageShow.h"

class CV_EXPORT MarkMatch
{
public:
	MarkMatch(void);
	~MarkMatch(void);

	bool RunMatch( float &x, float &y  );

	//bool SetImage(CMvImage &);
	bool SetImage(cv::Mat &_Image );

	bool SetTemp( std::string );
	bool GetCentor( cv::Mat &_TmpMat);
	//bool GetCentor();

	void (*Log_Add_wstring)(std::wstring, int level );
	void (*Log_Add_string)(std::string, int level );

	void Log_Add(std::wstring, int level=0 );
	void Log_Add(std::string, int level=0 );

public:
	bool UseMask;
	cv::Mat Mark; 
	cv::Mat MarkCanny; 
	cv::Mat Temp; 
	cv::Mat Mask; 
	cv::Mat Result;

	std::vector<cv::Moments> mu;
	std::vector<cv::Point2f> mc;

	cv::Point2f m_Centor;
};

#endif