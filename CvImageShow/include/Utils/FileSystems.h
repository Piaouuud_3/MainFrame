#pragma once

#include <algorithm>
#include <iomanip>
#include <fstream>
#include <memory>
#include <string>
#include <utility>
#include <vector>
#include <iostream>
#include <stdio.h>
#include <numeric>
#include <boost\filesystem.hpp>
#include <boost\format.hpp>     
#include <boost\tokenizer.hpp>     
#include <boost\algorithm\string.hpp>

namespace MyUtilitis
{
	namespace MyFileSystems
	{
		/*!
		 * \fn	void SplitExtFiler(std::string& strExtFilter, std::vector<std::string>& vectorExtNames);
		 *
		 * \brief	all extension names separated  by semicolon,  so we split a single input string to some string, must with the char '.'
		 *
		 * \param [in]	strExtFilter  	A filter specifying the extent. We split a single input string to some string, must with the char '.', such as ".bmp;.jpg"
		 * \param [in,out]	vectorExtNames	List of names of the vector extents.
		 */

		void SplitExtFiler(const std::string& strExtFilter, std::vector<std::string>& vectorExtNames);

		/*!
		 * \fn	* void ExchangeExtersionName(std::string& strPathName, std::string& strExtName);
		 *
		 * \brief	change the file extension
		 *
		 * \param [in,out]	strPathName	Full pathname of the file.
		 * \param [in]	strExtName 	The new file extension, must with the char '.', such as ".txt".
		 * 				
		 */
		void ExchangeExtersionName(std::string& strPathName, const std::string& strExtName);

		/*!
		 * \fn	int FindFilesInOneDirectory(std::string& strPathName, std::string& strExtFilter, std::vector<std::string>& vectorFiles);
		 *
		 * \brief	Find all files that conform to a particular type in the specified directory.
		 *
		 * \param [in,out]	strFolderName 	The specified directory, using full path name, not end with the char '\'.
		 * \param [in,out]	strExtFilter	The string list all you want type files, such as ".jpg;.bmp", if string equil empty, same as "*.*".
		 * \param [in,out]	vectorFiles 	All conformity to requirements files list in this vector, the filename is full path name, the original data will be deleted.
		 *
		 * \return	The found files in one directory.
		 */
		int FindFilesInOneDirectory(const std::string& strFolderName, const std::string& strExtFilter, std::vector<std::string>& vectorFiles);

		/*!
		 * \fn	bool Mkdir(std::string& strPathName);
		 *
		 * \brief	Mkdirs the given string path name
		 *
		 * \param [in,out]	strFolderName	Full pathname of the directory.
		 *
		 * \return	True if it succeeds, false if it fails.
		 */
		bool Mkdir(const std::string& strFolderName);

		/*!
		 * \fn	void DeleteTree(std::string& strPathName);
		 *
		 * \brief	Deletes the tree described by strPathName
		 *
		 * \param [in,out]	strFolderName	Full pathname of the directory.
		 */
		void DeleteTree(const std::string& strFolderName);
	}
}
