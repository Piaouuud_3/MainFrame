/*!
 * \file	CSingleton.h.
 *
 * \brief	Declares the singleton class
 * \code
 * 		class COneSingletonSample{
 * 		friend class CSigleton<COneSingletonSample>;
 * 		public:
 * 			void Inti();
 * 		private:
 * 			COneSingletonSample();
 * 		}
 *
 *		COneSingletonSample* pObject = CSingleton<COneSingletonSample>::instance();
 *		pObject->Init();
 * \endcode
 */

#pragma once

namespace MyUtilitis {

	template<class T>
	class CSingleton
	{
	public:
		static T* instance() {
			if (!m_pInstance) {
				m_pInstance = new T;
			}
			return m_pInstance;
		}
	protected:
		// hide the constructors
		CSingleton() { ; };
		CSingleton(const CSingleton&) { ; };
		CSingleton &operator = (const CSingleton&) { ; };
		virtual ~CSingleton()
		{
			if (m_pInstance)
			{
				delete m_pInstance;
				m_pInstance = nullptr;
			}
		}
	private:
		static T* m_pInstance;
	};
	template<class T> T* CSingleton<T>::m_pInstance = nullptr;
}

