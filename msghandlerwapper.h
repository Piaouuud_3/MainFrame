﻿#ifndef MSGHANDLERWAPPER_H
#define MSGHANDLERWAPPER_H

#include <QObject>

class MsgHandlerWapper:public QObject
{
    Q_OBJECT
public:
    static MsgHandlerWapper * instance();

signals:
    void LogMessageSig(const QtMsgType type, const QString current_date_time, const QString &msg);

private:
    MsgHandlerWapper();
    static MsgHandlerWapper * m_instance;

};


#endif // MSGHANDLERWAPPER_H
