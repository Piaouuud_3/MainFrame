﻿#include "mainui.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

//    QDir logPath("./QtLog/");
//    if( !logPath.exists() )
//    {
//        logPath.mkdir(".");
//    }

    //qInstallMessageHandler(outputMessage);
    MainUi w;
    w.show();

    return a.exec();
}
