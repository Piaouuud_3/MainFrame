﻿#ifndef MAINUI_H
#define MAINUI_H

#include <QMainWindow>

namespace Ui {
class MainUi;
}

class cvwidgetui;
class OcrTest;
class FeatureDetect;

class MainUi : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainUi(QWidget *parent = nullptr);
    ~MainUi();

    void InitCvWidget();

private slots:
    void on_tile_triggered();

public slots:
    void LogMessageslot(const QtMsgType type, const QString current_date_time, const QString &msg);

private:
    Ui::MainUi *ui;

    cvwidgetui *pCvwidgetui;
    OcrTest *pOcrTest;
    FeatureDetect *pFeatureDetect;

    unsigned int  LogCount;
};

#endif // MAINUI_H
