﻿#ifndef CHAISCRIPT_H
#define CHAISCRIPT_H

#include <opencv2/features2d.hpp>

#ifdef __WITH_CHAISCRIPT__
#include <chaiscript/chaiscript.hpp>
using namespace chaiscript;

extern ChaiScript chai;
#endif

void initFun_ChaiScript();

cv::Ptr<cv::FeatureDetector> GetDector_ChaiScript(const std::string);

#endif // CHAISCRIPT_H
